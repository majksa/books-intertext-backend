<?php declare(strict_types = 1);
#parse("PHP File Header.php")

namespace App\Model\Entity#if(${Relative_namespace} != "")\\${Relative_namespace}#end;

use App\Model\Database\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
final class ${NAME} extends Entity {

    public function __construct() {
    }

}
