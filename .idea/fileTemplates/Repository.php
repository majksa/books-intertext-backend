<?php declare(strict_types = 1);
#parse("PHP File Header.php")

namespace App\Model\Repository#if(${Relative_namespace} != "")\\${Relative_namespace}#end;

use App\Model\Database\Repository;
use App\Model\Entity#if(${Relative_namespace} != "")\\${Relative_namespace}#end\\${NAME} as ${NAME}Entity;
use App\Model\Exception\Runtime\Database\EntityNotFoundException;

/**
 * @extends Repository<${NAME}Entity>
 */
final class ${NAME} extends Repository {

}

