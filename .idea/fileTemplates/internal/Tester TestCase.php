<?php declare(strict_types = 1);

#if (${NAMESPACE})
namespace ${NAMESPACE};
#end

use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

#if (${BOOTSTRAP_RELATIVE_PATH})
require __DIR__ . '/${BOOTSTRAP_RELATIVE_PATH}';

#end

/** @testCase */
final class ${NAME} extends BaseTestCase {

}

(new ${NAME})->run();
