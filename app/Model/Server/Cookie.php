<?php declare(strict_types = 1);

namespace App\Model\Server;

use function time;

class Cookie {

    public function __construct(
        public ICookieManager $manager,
        public string $name,
        public string $value = '',
        public int $time = 0,
        public string $domain = '',
        public string $path = '/',
        public bool $secure = false,
    ) {
    }

    /**
     * Create or Update cookie.
     */
    public function save(): bool {
        return $this->manager->save($this);
    }

    /**
     * Return a cookie
     */
    public function get(): mixed {
        return $this->manager->get($this->name);
    }

    /**
     * Delete cookie.
     */
    public function delete(): bool {
        $this->time = time() - 60 * 60;

        return $this->manager->save($this);
    }

}
