<?php declare(strict_types = 1);

namespace App\Model\Server;

use App\Model\Entity\Security\UploadToken;
use InvalidArgumentException;
use Maxa\Ondrej\Nette\DI\Service;
use Nette\Http\RequestFactory;
use function file_exists;
use function mkdir;
use function move_uploaded_file;

#[Service(setup: '$object->setFolder($container->getParameters()["wwwDir"]);')]
final class FileManager {

    public const MAX_SIZE = 134_217_728; // 128 MB

    public const FOLDER = '/uploads';

    public string $folder;

    public function setFolder(string $root): void {
        $folder = $root . self::FOLDER;
        if (!file_exists($folder)) {
            mkdir($folder);
        }

        $this->folder = $folder;
    }

    public function getPath(string $file): string {
        return "$this->folder/$file";
    }

    public static function getLink(string $file): string {
        return (new RequestFactory())->fromGlobals()->url->hostUrl . self::FOLDER . "/$file";
    }

    public function save(string $tmpFile, int $size, UploadToken $token): void {
        if ($size >= self::MAX_SIZE) {
            throw new InvalidArgumentException('File size must be smaller than ' . self::MAX_SIZE);
        }

        move_uploaded_file($tmpFile, $this->getPath($token->token));
    }

}
