<?php declare(strict_types = 1);

namespace App\Model\Utils;

use function array_key_exists;
use function array_search;
use function is_int;
use function is_string;

class Arrays extends \Nette\Utils\Arrays {

    /**
     * Removes and item from array.
     *
     * @param array<mixed> $array
     */
    public static function remove(array &$array, mixed $key): void {
        unset($array[$key]);
    }

    /**
     * Searches for item in array and returns its key.
     *
     * @param array<mixed> $array
     * @param bool $strict [optional] <p>
     * If the third parameter strict is set to true
     * then the array_search function will also check the
     * types of the
     * needle in the haystack.
     */
    public static function search(array $array, mixed $value, bool $strict = true): int|string|false {
        $key = array_search($value, $array, $strict);
        if (is_string($key) || is_int($key)) {
            return $key;
        }

        return false;
    }

    /**
     * If array contains the provided key.
     *
     * @param array<mixed> $array
     */
    public static function keyExists(array $array, mixed $key): bool {
        return array_key_exists($key, $array);
    }

}
