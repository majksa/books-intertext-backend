<?php declare(strict_types = 1);

namespace App\Model\Security;

use App\Model\Entity\Security\User;
use Maxa\Ondrej\Nette\GraphQL\DI\Authentication;
use TheCodingMachine\GraphQLite\Security\AuthenticationServiceInterface;

#[Authentication]
final class AuthenticationService implements AuthenticationServiceInterface {

    public function __construct(private Security $security) {
    }

    public function getUser(): ?User {
        return $this->security->getLogged();
    }

    public function isLogged(): bool {
        return $this->getUser() instanceof User;
    }

}
