<?php declare(strict_types = 1);

namespace App\Model\Security;

use App\Model\Database\EntityManager;
use App\Model\Entity\Security\Permission;
use App\Model\Entity\Security\RegistrationToken;
use App\Model\Entity\Security\Token;
use App\Model\Entity\Security\UploadToken;
use App\Model\Entity\Security\User;
use App\Model\Server\Cookie;
use App\Model\Server\ICookieManager;
use DateTimeImmutable;
use Maxa\Ondrej\Nette\DI\Service;
use Nette\InvalidStateException;
use Nette\Utils\Random;
use function assert;
use function is_string;
use function openssl_decrypt;
use function openssl_encrypt;
use function time;
use function uniqid;

#[Service('security')]
class Security {

    public const COOKIE = 'oauth-token';

    private const ENCRYPTION_METHOD = 'aes-128-ctr';

    private const IV = 'Hp^qpe@9K6iRk8tx';

    private const PASSPHRASE = 'X6iivexFW6Qw26LMnKDaXr5VowLEw2crBhTDtsMVtWPUf6MUeFVDA6GBN69Vk4LQJ3A3gB8MNooiJ3UYH4Za6TwYmZR9HE43cW39';

    private Passwords $passwords;

    public Cookie $cookie;

    public function __construct(private EntityManager $em, ICookieManager $cookieManager) {
        $this->passwords = new Passwords();
        $this->cookie = $cookieManager->new(self::COOKIE);
    }

    public function getLogged(): ?User {
        $tokenHash = $this->cookie->get();
        if (!is_string($tokenHash)) {
            return null;
        }

        $token = openssl_decrypt($tokenHash, self::ENCRYPTION_METHOD, self::PASSPHRASE, iv: self::IV);
        $entity = $this->em->getTokenRepository()->findOneByToken($token);
        if ($entity instanceof Token) {
            return $entity->user;
        }

        $this->cookie->delete();

        return null;
    }

    /**
     * @return array<User>
     */
    public function getUsers(): array {
        return $this->em->getUserRepository()->findAll();
    }

    public function isUserAllowed(User $user, string $right): bool {
        $permission = $this->em->getPermissionRepository()->findOneByName($right);
        if ($permission instanceof Permission) {
            return $user->isAllowed($permission);
        }

        return false;
    }

    /**
     * @throws AuthenticationException
     */
    public function authenticate(string $username, string $password): User {
        $user = $this->em->getUserRepository()->findOneByUsername($username);
        if (!($user instanceof User)) {
            throw new AuthenticationException('User with the specified username does not exist.');
        }

        if ($user->password === null) {
            throw new AuthenticationException('Password is not set. Please login using FIDO.');
        }

        if (!$this->passwords->verify($password, $user->password)) {
            throw new AuthenticationException('Invalid password.');
        }

        return $user;
    }

    public function login(User $user): void {
        $user->updateLoggedAt();
        $this->em->flush();
        $hash = openssl_encrypt(
            $this->generateToken($user)->token,
            self::ENCRYPTION_METHOD,
            self::PASSPHRASE,
            iv: self::IV,
        );
        assert(is_string($hash));
        $this->cookie->value = $hash;
        $this->cookie->time = time() + 3_600;
        $this->cookie->save();
    }

    public function generateToken(User $user): Token {
        do {
            $token = Random::generate(100);
        } while ($this->em->getTokenRepository()->isTokenTaken($token));

        $entity = new Token($user, $token);
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    public function generateRegistrationToken(): RegistrationToken {
        $entity = new RegistrationToken(uniqid('reg_tkn_'), new DateTimeImmutable('+1 day'));
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    /**
     * @throws InvalidStateException thrown if the token is not valid
     */
    public function validateRegistrationToken(string $token): RegistrationToken {
        $entity = $this->em->getRegistrationTokenRepository()->findOneByToken($token);
        if (!($entity instanceof RegistrationToken)) {
            throw new InvalidStateException('Token does not exist.', 404);
        }

        if ($entity->used) {
            throw new InvalidStateException('Token has already been used.', 409);
        }

        if (time() > $entity->expiresAt->getTimestamp()) {
            throw new InvalidStateException('Token is expired.', 409);
        }

        return $entity;
    }

    /**
     * @return array<UploadToken>
     */
    public function generateUploadTokens(int $limit): array {
        $tokens = [];
        for ($i = 0; $i < $limit; $i++) {
            $entity = new UploadToken(uniqid('upl_tkn_'), new DateTimeImmutable('+1 day'));
            $this->em->persist($entity);
            $this->em->flush($entity);
            $tokens[] = $entity;
        }

        $this->em->flush();

        return $tokens;
    }

    /**
     * @throws InvalidStateException thrown if the token is not valid
     */
    public function validateUploadToken(string $token): UploadToken {
        $entity = $this->em->getUploadTokenRepository()->findOneByToken($token);
        if (!($entity instanceof UploadToken)) {
            throw new InvalidStateException('Token does not exist.', 404);
        }

        if ($entity->used) {
            throw new InvalidStateException('Token has already been used.', 409);
        }

        if (time() > $entity->expiresAt->getTimestamp()) {
            throw new InvalidStateException('Token is expired.', 409);
        }

        $entity->used = true;
        $this->em->flush();

        return $entity;
    }

    public function logout(): void {
        $this->cookie->delete();
    }

    /**
     * @throws UsernameTakenException
     * @throws EmailAlreadyInUseException
     */
    public function register(User $user): void {
        $this->em->getUserRepository()->checkIfUsernameOrEmailTaken($user->username, $user->email);
        if ($user->password !== null) {
            $user->password = $this->passwords->hash($user->password);
        }

        $this->em->persist($user);
        $this->em->flush();
    }

}
