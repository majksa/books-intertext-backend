<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use App\Model\Database\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table(name: 'book')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Book extends Entity {

    /** @var Collection<int,BookRelationConnection> */
    #[ORM\OneToMany(mappedBy: 'book', targetEntity: BookRelationConnection::class)]
    public Collection $relation_connections;

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $title,
        #[Field]
        #[ORM\ManyToOne(targetEntity: Author::class, inversedBy: 'books')]
        #[ORM\JoinColumn(name: 'author_id', referencedColumnName: 'id')]
        public Author $author,
    ) {
        $this->relation_connections = new ArrayCollection();
    }

    /**
     * @return array<BookRelationConnection>
     */
    #[Field]
    public function getRelations(): array {
        return $this->relation_connections->toArray();
    }

}
