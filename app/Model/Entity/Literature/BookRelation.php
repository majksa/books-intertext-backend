<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use App\Model\Database\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table(name: 'book_relation')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class BookRelation extends Entity {

    /** @var Collection<int,BookRelationConnection> */
    #[ORM\OneToMany(mappedBy: 'relation', targetEntity: BookRelationConnection::class)]
    public Collection $relation_connections;

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $name,
    ) {
        $this->relation_connections = new ArrayCollection();
    }

    /**
     * @return array<BookRelationConnection>
     */
    #[Field]
    public function getBooks(): array {
        return $this->relation_connections->toArray();
    }

}
