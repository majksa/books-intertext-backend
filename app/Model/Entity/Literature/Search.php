<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
final class Search {

    /** @var array<Book> */
    #[Field]
    public array $books = [];

    /** @var array<Author> */
    #[Field]
    public array $authors = [];

    /** @var array<BookRelation> */
    #[Field]
    public array $relations = [];

}
