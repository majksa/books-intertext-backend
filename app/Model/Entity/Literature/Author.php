<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use App\Model\Database\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table(name: 'author')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Author extends Entity {

    /** @var Collection<int,Book> */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Book::class)]
    public Collection $books;

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $name,
    ) {
        $this->books = new ArrayCollection();
    }

    public function addBook(Book $book): void {
        $this->books[] = $book;
        $book->author = $this;
    }

    /**
     * @return array<Book>
     */
    #[Field]
    public function getBooks(): array {
        return $this->books->toArray();
    }

}
