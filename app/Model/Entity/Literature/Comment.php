<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use App\Model\Database\Entity;
use App\Model\Entity\Security\User;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table(name: 'book_relation_comment')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Comment extends Entity {

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\ManyToOne(targetEntity: User::class)]
        #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
        public User $user,
        #[Field]
        #[ORM\ManyToOne(targetEntity: BookRelationConnection::class, inversedBy: 'comments')]
        #[ORM\JoinColumn(name: 'relation_connection_id', referencedColumnName: 'id')]
        public BookRelationConnection $relation,
        #[Field]
        #[ORM\Column(type: Types::TEXT)]
        public string $message,
    ) {
    }

}
