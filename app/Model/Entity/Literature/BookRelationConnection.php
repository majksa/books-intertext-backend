<?php declare(strict_types = 1);

namespace App\Model\Entity\Literature;

use App\Model\Database\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table(name: 'book_relation_connection')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class BookRelationConnection extends Entity {

    /** @var Collection<int,Vote> */
    #[ORM\OneToMany(mappedBy: 'relation', targetEntity: Vote::class)]
    public Collection $votes;

    /** @var Collection<int,Comment> */
    #[ORM\OneToMany(mappedBy: 'relation', targetEntity: Comment::class)]
    public Collection $comments;

    #[Pure]
    public function __construct(
        #[Field]
        #[ORM\ManyToOne(targetEntity: BookRelation::class, inversedBy: 'relation_connections')]
        #[ORM\JoinColumn(name: 'relation_id', referencedColumnName: 'id')]
        public BookRelation $relation,
        #[Field]
        #[ORM\ManyToOne(targetEntity: Book::class, inversedBy: 'relation_connections')]
        #[ORM\JoinColumn(name: 'book_id', referencedColumnName: 'id')]
        public Book $book,
    ) {
        $this->votes = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @return array<Vote>
     */
    #[Field]
    public function getVotes(): array {
        return $this->votes->toArray();
    }

    /**
     * @return array<Comment>
     */
    #[Field]
    public function getComments(): array {
        return $this->comments->toArray();
    }

}
