<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;

#[ORM\Table(name: 'fido')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Fido extends Entity {

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'fidos')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    public User $user;

    #[Pure]
    public function __construct(
        #[ORM\Column(type: Types::STRING, length: 255)]
        public string $handle,
        #[ORM\Column(type: Types::STRING, length: 255)]
        public string $public,
        #[ORM\Column(type: Types::TEXT)]
        public string $certificate,
        #[ORM\Column(type: Types::INTEGER)]
        public int $counter,
    ) {
    }

}
