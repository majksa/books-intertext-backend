<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use App\Model\Repository\Security\RegistrationToken as RegistrationTokenRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table('registration_token')]
#[ORM\Entity(repositoryClass: RegistrationTokenRepository::class)]
#[ORM\HasLifecycleCallbacks]
class RegistrationToken extends Entity {

    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $token,
        #[Field]
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
        public DateTimeImmutable $expiresAt,
        #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
        public bool $used = false,
    ) {
    }

}
