<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Repository\Security\User as UserRepository;
use CodeLts\U2F\U2FServer\Registration;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use stdClass;
use Symfony\Component\Validator\Constraints as Assert;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use function md5;

/**
 * @extends PermissionHolder<UserPermissionBinding>
 */
#[Type]
#[ORM\Table(name: 'user')]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\HasLifecycleCallbacks]
class User extends PermissionHolder {

    /** @var Collection<int,UserPermissionBinding> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserPermissionBinding::class)]
    public Collection $bindings;

    #[Field]
    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    public ?DateTimeImmutable $lastLoggedAt = null;

    /** @var Collection<int,Role> */
    #[ORM\ManyToMany(targetEntity: Role::class, inversedBy: 'users')]
    public Collection $roles;

    /** @var Collection<int,Fido> */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Fido::class)]
    public Collection $fidos;

    #[Pure]
    public function __construct(
        #[Field]
        #[Assert\Email]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $email,
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $username,
        #[ORM\Column(type: Types::STRING, length: 255, nullable: true)]
        public ?string $password,
    ) {
        $this->bindings = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->fidos = new ArrayCollection();
    }

    public function updateLoggedAt(): void {
        $this->lastLoggedAt = new DateTimeImmutable();
    }

    /**
     * @return Collection<int,UserPermissionBinding>
     */
    #[Pure]
    public function getBindings(): Collection {
        return $this->bindings;
    }

    public function create(Permission $permission): UserPermissionBinding {
        return $this->bindings[] = new UserPermissionBinding($this, $permission);
    }

    public function addFido(Registration $registration): void {
        $fido = new Fido(
            $registration->getKeyHandle(),
            $registration->getPublicKey(),
            $registration->getCertificate(),
            (int) $registration->getCounter(),
        );
        $this->fidos[] = $fido;
        $fido->user = $this;
    }

    /**
     * @return array<stdClass>
     */
    #[Pure]
    public function getFidoHandles(): array {
        $handles = [];
        foreach ($this->fidos as $fido) {
            $obj = new stdClass();
            $obj->keyHandle = $fido->handle;
            $handles[] = $obj;
        }

        return $handles;
    }

    #[Pure]
    public function getValue(Permission $permission): int {
        $value = parent::getValue($permission);
        if ($value !== PermissionBinding::DEFAULT) {
            return $value;
        }

        foreach ($this->roles as $role) {
            if ($role->isAllowed($permission)) {
                return PermissionBinding::ALLOW;
            }
        }

        return PermissionBinding::DEFAULT;
    }

    #[Pure]
    public function isAllowed(Permission $permission): bool {
        return $this->getValue($permission) === PermissionBinding::ALLOW;
    }

    #[Field]
    public function getGravatar(): string {
        return 'https://www.gravatar.com/avatar/' . md5($this->email);
    }

    public function addRole(Role $role): void {
        $role->addUser($this);
        $this->roles[] = $role;
    }

    public function removeRole(Role $role): void {
        $role->removeUser($this);
        $this->roles->removeElement($role);
    }

}
