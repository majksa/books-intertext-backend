<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use App\Model\Repository\Security\UploadToken as UploadTokenRepository;
use App\Model\Server\FileManager;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
#[ORM\Table('upload_token')]
#[ORM\Entity(UploadTokenRepository::class)]
#[ORM\HasLifecycleCallbacks]
class UploadToken extends Entity {

    #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
    public bool $used = false;

    public function __construct(
        #[Field]
        #[ORM\Column(type: Types::STRING, length: 255, unique: true)]
        public string $token,
        #[Field]
        #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
        public DateTimeImmutable $expiresAt,
    ) {
    }


    #[Field(name: 'link')]
    public function getLink(): string {
        return FileManager::getLink($this->token);
    }

}
