<?php declare(strict_types = 1);

namespace App\Model\Entity\Security;

use App\Model\Database\Entity;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/**
 * @template H of PermissionHolder
 */
#[Type]
#[ORM\MappedSuperclass]
abstract class PermissionBinding extends Entity {

    public const ALLOW = 1;

    public const DEFAULT = 0;

    public const DENY = -1;

    public function __construct(
        #[Field]
        #[ORM\ManyToOne(targetEntity: Permission::class)]
        #[ORM\JoinColumn(name: 'permission_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
        public Permission $permission,
        #[Field]
        #[ORM\Column(type: Types::SMALLINT)]
        public int $value = self::DEFAULT,
    ) {
    }

    /**
     * @return H
     */
    #[Pure]
    abstract public function getHolder(): PermissionHolder;

}
