<?php declare(strict_types = 1);

namespace App\Model\Repository;

use App\Model\Database\Entity;
use App\Model\Database\EntityManager;
use App\Model\Database\Repository;
use App\Model\Entity\Security\Permission;
use App\Model\Entity\Security\RegistrationToken;
use App\Model\Entity\Security\Token;
use App\Model\Entity\Security\UploadToken;
use App\Model\Entity\Security\User;
use App\Model\Repository\Security\Permission as PermissionRepository;
use App\Model\Repository\Security\RegistrationToken as RegistrationTokenRepository;
use App\Model\Repository\Security\Token as TokenRepository;
use App\Model\Repository\Security\UploadToken as UploadTokenRepository;
use App\Model\Repository\Security\User as UserRepository;
use function assert;

/**
 * @mixin EntityManager
 */
trait TRepositories {

    /**
     * @param class-string<E> $entityClass
     * @param class-string<R> $repositoryClass
     * @return R
     *
     * @template E of Entity
     * @template R of Repository
     * @internal
     */
    public function repository(string $entityClass, string $repositoryClass): Repository {
        $repository = $this->getRepository($entityClass);
        assert($repository instanceof $repositoryClass);

        return $repository;
    }

    public function getUserRepository(): UserRepository {
        return $this->repository(User::class, UserRepository::class);
    }

    public function getPermissionRepository(): PermissionRepository {
        return $this->repository(Permission::class, PermissionRepository::class);
    }

    public function getTokenRepository(): TokenRepository {
        return $this->repository(Token::class, TokenRepository::class);
    }

    public function getRegistrationTokenRepository(): RegistrationTokenRepository {
        return $this->repository(RegistrationToken::class, RegistrationTokenRepository::class);
    }

    public function getUploadTokenRepository(): UploadTokenRepository {
        return $this->repository(UploadToken::class, UploadTokenRepository::class);
    }

}
