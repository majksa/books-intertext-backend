<?php declare(strict_types = 1);

namespace App\Model\Repository\Security;

use App\Model\Database\Repository;
use App\Model\Entity\Security\Token as TokenEntity;
use App\Model\Entity\Security\User;

/**
 * @extends Repository<TokenEntity>
 * @method TokenEntity|null findOneByUser(User $user)
 * @method TokenEntity|null findOneByToken(string $token)
 */
final class Token extends Repository {

    public function isTokenTaken(string $token): bool {
        return $this->count(['token' => $token]) > 0;
    }

}
