<?php

declare(strict_types = 1);

namespace App\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504193319 extends AbstractMigration {

    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO role (`id`, `name`) VALUES (2, \'everyone\')');
        $this->addSql('
        CREATE TRIGGER `add_everyone_role` AFTER INSERT ON `user` FOR EACH ROW
        INSERT INTO user_role (`role_id`, `user_id`) VALUES (2, NEW.id)
        ');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please    modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TRIGGER `add_everyone_role`');
        $this->addSql('DELETE FROM role WHERE name=\'everyone\'');
    }
}
