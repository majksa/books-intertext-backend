<?php

declare(strict_types = 1);

namespace App\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220420200048 extends AbstractMigration {

    public function getDescription(): string {
        return 'Roles and permissions';
    }

    public function up(Schema $schema): void {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE role_permission_binding DROP FOREIGN KEY FK_4905D038FED90CCA');
        $this->addSql('ALTER TABLE role_permission_binding ADD CONSTRAINT FK_4905D038FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_permission_binding DROP FOREIGN KEY FK_C8033089FED90CCA');
        $this->addSql('ALTER TABLE user_permission_binding ADD CONSTRAINT FK_C8033089FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id) ON DELETE CASCADE');

        $this->addSql('INSERT INTO role (`id`, `name`) VALUES (1, \'admin\')');
        $this->addSql('
        CREATE TRIGGER `create_admin_perms` AFTER INSERT ON `permission` FOR EACH ROW
        INSERT INTO role_permission_binding (`role_id`, `permission_id`, `value`) VALUES (1, NEW.id, 1)
        ');
        $this->addSql('INSERT INTO permission (`name`, `description`) VALUES (\'CREATE_REGISTRATION\', \'Holder can create new registration tokens used to add new users into the application.\')');
    }

    public function down(Schema $schema): void {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TRIGGER `creat_admin_perms`');
        $this->addSql('DELETE FROM permission WHERE `name` = \'CREATE_REGISTRATION\'');
        $this->addSql('DELETE FROM role WHERE name=\'admin\'');

        $this->addSql('ALTER TABLE role_permission_binding DROP FOREIGN KEY FK_4905D038FED90CCA');
        $this->addSql('ALTER TABLE role_permission_binding ADD CONSTRAINT FK_4905D038FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id)');
        $this->addSql('ALTER TABLE user_permission_binding DROP FOREIGN KEY FK_C8033089FED90CCA');
        $this->addSql('ALTER TABLE user_permission_binding ADD CONSTRAINT FK_C8033089FED90CCA FOREIGN KEY (permission_id) REFERENCES permission (id)');
    }
}
