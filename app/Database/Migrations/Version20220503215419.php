<?php

declare(strict_types = 1);

namespace App\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503215419 extends AbstractMigration {

    public function getDescription(): string {
        return '';
    }

    public function up(Schema $schema): void {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO permission (name, description) 
            VALUES (\'CREATE_BOOK\', NULL),
                   (\'LIST_BOOKS\', NULL),
                   (\'CREATE_AUTHOR\', NULL),
                   (\'LIST_AUTHORS\', NULL),
                   (\'LIST_RELATIONS\', NULL),
                   (\'CREATE_RELATION\', NULL),
                   (\'REACT_TO_RELATION\', NULL)
       ');
    }

    public function down(Schema $schema): void {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM permission WHERE name IN (
            \'CREATE_BOOK\',
            \'LIST_BOOKS\',
            \'CREATE_AUTHOR\',
            \'LIST_AUTHORS\',
            \'LIST_RELATIONS\',
            \'CREATE_RELATION\',
            \'REACT_TO_RELATION\'
        )');
    }
}
