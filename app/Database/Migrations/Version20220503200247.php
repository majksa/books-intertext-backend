<?php

declare(strict_types=1);

namespace App\Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220503200247 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE book_relation_comment (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, relation_connection_id INT DEFAULT NULL, message LONGTEXT NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_by VARCHAR(255) DEFAULT NULL, updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_by VARCHAR(255) DEFAULT NULL, INDEX IDX_4A5C5F3BA76ED395 (user_id), INDEX IDX_4A5C5F3B78479AE5 (relation_connection_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE book_relation_vote (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, relation_connection_id INT DEFAULT NULL, upvote TINYINT(1) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', created_by VARCHAR(255) DEFAULT NULL, updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_by VARCHAR(255) DEFAULT NULL, INDEX IDX_88D3BEE5A76ED395 (user_id), INDEX IDX_88D3BEE578479AE5 (relation_connection_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE book_relation_comment ADD CONSTRAINT FK_4A5C5F3BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE book_relation_comment ADD CONSTRAINT FK_4A5C5F3B78479AE5 FOREIGN KEY (relation_connection_id) REFERENCES book_relation_connection (id)');
        $this->addSql('ALTER TABLE book_relation_vote ADD CONSTRAINT FK_88D3BEE5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE book_relation_vote ADD CONSTRAINT FK_88D3BEE578479AE5 FOREIGN KEY (relation_connection_id) REFERENCES book_relation_connection (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE book_relation_comment');
        $this->addSql('DROP TABLE book_relation_vote');
    }
}
