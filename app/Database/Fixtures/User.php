<?php declare(strict_types = 1);

namespace App\Database\Fixtures;

use App\Model\Entity\Security\Role;
use App\Model\Entity\Security\User as UserEntity;
use App\Model\Security\Passwords;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Nelmio\Alice\Throwable\LoadingThrowable;

final class User extends Fixture {

    private ObjectManager $manager;

    public function getOrder(): int {
        return 1;
    }

    /**
     * @throws LoadingThrowable
     */
    public function load(ObjectManager $manager): void {
        $this->manager = $manager;

        foreach ($this->getStaticUsers() as $user) {
            $this->saveUser($user, $user['admin']);
        }

        foreach ($this->getRandomUsers() as $user) {
            $this->manager->persist($user);
        }

        $this->manager->flush();
    }

    /**
     * @return mixed[]
     */
    protected function getStaticUsers(): iterable {
        yield [
            'email' => 'admin@backend.cz',
            'username' => 'admin',
            'admin' => true,
        ];
    }

    /**
     * @return User[]
     * @throws LoadingThrowable
     */
    protected function getRandomUsers(): iterable {
        $loader = new ReflectionLoader(Factory::create('cs_CZ'));
        $objectSet = $loader->loadData([
            UserEntity::class => [
                'user{1..100}' => [
                    '__construct' => [
                        '<email()>',
                        '<username()>',
                        '<password()>',
                    ],
                ],
            ],
        ]);

        $objects = $objectSet->getObjects();
        $i = 1;
        $passwords = new Passwords();
        foreach ($objects as $object) {
            $object->id = $i++;
            $object->password = $passwords->hash($object->password);
        }

        return $objects;
    }

    /**
     * @param mixed[] $user
     */
    protected function saveUser(array $user, bool $admin): void {
        $entity = new UserEntity(
            $user['email'],
            $user['username'],
            (new Passwords())->hash('admin')
        );

        if ($admin) {
            $role = $this->manager->find(Role::class, 1);
            if ($role instanceof Role) {
                $entity->addRole($role);
            }
        }

        $this->manager->persist($entity);
    }

}