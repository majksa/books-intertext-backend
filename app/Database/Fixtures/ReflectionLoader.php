<?php declare(strict_types = 1);

namespace App\Database\Fixtures;

use Nelmio\Alice\Loader\NativeLoader;
use Nelmio\Alice\PropertyAccess\ReflectionPropertyAccessor;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ReflectionLoader extends NativeLoader {

    protected function createPropertyAccessor(): PropertyAccessorInterface {
        return new ReflectionPropertyAccessor(
            PropertyAccess::createPropertyAccessorBuilder()
                ->enableMagicCall()
                ->getPropertyAccessor()
        );
    }

}