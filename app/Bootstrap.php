<?php declare(strict_types = 1);

namespace App;

use Contributte\Bootstrap\ExtraConfigurator;
use Nette\Configurator;
use Nette\DI\Compiler;
use Tracy\Debugger;
use function explode;
use function file_exists;
use function filter_input;
use function getenv;
use function header;
use function in_array;
use function realpath;
use const INPUT_SERVER;

class Bootstrap {

    public static function boot(): Configurator {
        $configurator = new ExtraConfigurator();
        $configurator->setTempDirectory(__DIR__ . '/../temp');

        // Disable default extensions
        unset($configurator->defaultExtensions['security']);

        $configurator->onCompile[] = static function (ExtraConfigurator $configurator, Compiler $compiler): void {
            // Add env variables to config structure
            $compiler->addConfig(['parameters' => $configurator->getEnvironmentParameters()]);
        };

        // According to NETTE_DEBUG env
        $configurator->setEnvDebugMode();

        self::cors();

        // Enable tracy and configure it
        $configurator->enableTracy(__DIR__ . '/../log');

        if (($editor = getenv('NETTE_EDITOR', true)) !== false) {
            Debugger::$editor = $editor;
        }

        // Provide some parameters
        $configurator->addParameters([
            'rootDir' => realpath(__DIR__ . '/..'),
            'appDir' => __DIR__,
            'wwwDir' => realpath(__DIR__ . '/../www'),
            'testMode' => false,
        ]);

        // Load development or production config
        $env = getenv('NETTE_ENV', true);
        if ($env === false && Debugger::$productionMode || $env === 'prod') {
            $configurator->addConfig(__DIR__ . '/../config/env/prod.neon');
        } else {
            $configurator->addConfig(__DIR__ . '/../config/env/dev.neon');
        }

        if (file_exists(__DIR__ . '/../config/local.neon')) {
            $configurator->addConfig(__DIR__ . '/../config/local.neon');
        }

        return $configurator;
    }

    private static function cors(): void {
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: Origin, Content-Type');

        // Handle Origin
        $allowed = getenv('CORS_ORIGIN', true);
        $origin = filter_input(INPUT_SERVER, 'HTTP_ORIGIN');

        if ($origin === null) {
            return;
        }

        if ($allowed !== false && $allowed !== '*') {
            if (!in_array($origin, explode(';', $allowed), true)) {
                // Fail the CORS check
                header('Access-Control-Allow-Origin: https://example.com');
                echo "You are not allowed to access this website from origin: $origin";
                exit(0);
            }
        }

        header("Origin: $origin");
        header("Access-Control-Allow-Origin: $origin");
    }

}
