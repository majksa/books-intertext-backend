<?php declare(strict_types = 1);

namespace App\GraphQL\Controller;

use App\GraphQL\Input\UserFactory;
use App\GraphQL\Rights;
use App\Model\Database\EntityManager;
use App\Model\Entity\Security\RegistrationToken;
use App\Model\Entity\Security\User;
use App\Model\Security\AuthenticationException;
use App\Model\Security\EmailAlreadyInUseException;
use App\Model\Security\Security;
use App\Model\Security\UsernameTakenException;
use Nette\InvalidStateException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Right;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;
use TheCodingMachine\GraphQLite\Validator\ValidationFailedException;

final class UserController {

    public function __construct(
        private Security $security,
        private ValidatorInterface $validator,
        private EntityManager $em,
    ) {
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation]
    public function login(string $username, string $password): User {
        try {
            $user = $this->security->authenticate($username, $password);
            $this->security->login($user);

            return $user;
        } catch (AuthenticationException $e) {
            throw new GraphQLException($e->getMessage(), 401, $e, 'Unauthorized');
        }
    }

    /**
     * @throws GraphQLException
     */
    #[Mutation]
    public function register(#[UseInputType(UserFactory::CREATE)] User $user, string $token): User {
        ValidationFailedException::throwException($this->validator->validate($user));
        if ($user->password === null) {
            throw new GraphQLException('Password has not been set.');
        }

        try {
            $tokenEntity = $this->security->validateRegistrationToken($token);
        } catch (InvalidStateException $e) {
            throw new GraphQLException(
                $e->getMessage(),
                $e->getCode(),
                $e,
                $e->getCode() === 404 ? 'Not Found' : 'Conflict',
            );
        }

        try {
            $this->security->register($user);
            $tokenEntity->used = true;
            $this->em->flush();
        } catch (EmailAlreadyInUseException | UsernameTakenException $e) {
            throw new GraphQLException($e->getMessage(), 409, $e, 'Conflict');
        }

        return $user;
    }

    #[Logged]
    #[Right(Rights::CREATE_REGISTRATION)]
    #[Mutation]
    public function createRegistration(): RegistrationToken {
        return $this->security->generateRegistrationToken();
    }

    #[Logged]
    #[Mutation]
    public function logout(): bool {
        $this->security->logout();

        return true;
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Query]
    public function me(): User {
        if (($user = $this->security->getLogged()) === null) {
            throw new GraphQLException('You need to be logged in', 401, category: 'Unauthorized');
        }

        return $user;
    }

}
