<?php declare(strict_types = 1);

namespace App\GraphQL\Controller;

use App\GraphQL\Rights;
use App\Model\Database\EntityManager;
use App\Model\Entity\Literature\Author;
use App\Model\Entity\Literature\Book;
use App\Model\Entity\Literature\BookRelation;
use App\Model\Entity\Literature\BookRelationConnection;
use App\Model\Entity\Literature\Comment;
use App\Model\Entity\Literature\Search;
use App\Model\Entity\Literature\Vote;
use App\Model\Entity\Security\User;
use App\Model\Security\Security;
use TheCodingMachine\GraphQLite\Annotations\Logged;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Right;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLException;
use function assert;

final class LiteratureController {

    public function __construct(private EntityManager $em, private Security $security) {
    }

    /**
     * @return array<Book>
     */
    #[Logged]
    #[Right(Rights::LIST_BOOKS)]
    #[Query(name: 'books')]
    public function listBooks(): array {
        return $this->em->getRepository(Book::class)->findAll();
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::LIST_BOOKS)]
    #[Query(name: 'book')]
    public function getBook(int $id): Book {
        $repository = $this->em->getRepository(Book::class);
        if (($entity = $repository->find($id)) instanceof Book) {
            return $entity;
        }

        throw new GraphQLException("Book with the provided id '$id' was not found!", 404, category: 'Not Found');
    }

    #[Logged]
    #[Right(Rights::CREATE_BOOK)]
    #[Mutation(name: 'createBook')]
    public function createBook(string $title, string $author): Book {
        if (!(($authorEntity = $this->em->getRepository(Author::class)->findOneBy(
            ['name' => $author],
        )) instanceof Author)) {
            $authorEntity = new Author($author);
            $this->em->persist($authorEntity);
        }

        $book = new Book($title, $authorEntity);
        $this->em->persist($book);
        $this->em->flush();

        return $book;
    }

    /**
     * @return array<Author>
     */
    #[Logged]
    #[Right(Rights::LIST_AUTHORS)]
    #[Query(name: 'authors')]
    public function listAuthors(): array {
        return $this->em->getRepository(Author::class)->findAll();
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::LIST_AUTHORS)]
    #[Query(name: 'author')]
    public function getAuthor(int $id): Author {
        $repository = $this->em->getRepository(Author::class);
        if (($entity = $repository->find($id)) instanceof Author) {
            return $entity;
        }

        throw new GraphQLException("Author with the provided id '$id' was not found!", 404, category: 'Not Found');
    }

    #[Logged]
    #[Right(Rights::CREATE_AUTHOR)]
    #[Mutation(name: 'createAuthor')]
    public function createAuthor(string $name): Author {
        $author = new Author($name);
        $this->em->persist($author);
        $this->em->flush();

        return $author;
    }

    /**
     * @return array<BookRelation>
     */
    #[Logged]
    #[Right(Rights::LIST_RELATIONS)]
    #[Query(name: 'relations')]
    public function listRelations(): array {
        return $this->em->getRepository(BookRelation::class)->findAll();
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::LIST_RELATIONS)]
    #[Query(name: 'relation')]
    public function getRelation(int $id): BookRelation {
        $repository = $this->em->getRepository(BookRelation::class);
        if (($entity = $repository->find($id)) instanceof BookRelation) {
            return $entity;
        }

        throw new GraphQLException(
            "Book Relation with the provided id '$id' was not found!",
            404,
            category: 'Not Found',
        );
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::LIST_RELATIONS)]
    #[Query(name: 'relationConnection')]
    public function getRelationConnection(int $id): BookRelationConnection {
        $repository = $this->em->getRepository(BookRelationConnection::class);
        if (($entity = $repository->find($id)) instanceof BookRelationConnection) {
            return $entity;
        }

        throw new GraphQLException(
            "Book Relation Connection with the provided id '$id' was not found!",
            404,
            category: 'Not Found',
        );
    }

    #[Logged]
    #[Right(Rights::CREATE_RELATION)]
    #[Mutation(name: 'createRelation')]
    public function createRelation(string $name): BookRelation {
        $relation = new BookRelation($name);
        $this->em->persist($relation);
        $this->em->flush();

        return $relation;
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::CREATE_RELATION)]
    #[Mutation(name: 'addBookRelation')]
    public function addBookRelation(string $relationName, int $bookId): BookRelationConnection {
        if (($book = $this->em->find(Book::class, $bookId)) instanceof Book) {
            if (!(($relation = $this->em->getRepository(BookRelation::class)->findOneBy(
                ['name' => $relationName],
            )) instanceof BookRelation)) {
                $relation = new BookRelation($relationName);
                $this->em->persist($relation);
            }

            $connection = new BookRelationConnection($relation, $book);
            $this->em->persist($connection);
            $this->em->flush();

            return $connection;
        }

        throw new GraphQLException("Book with the provided id '$bookId' was not found!", 404, category: 'Not Found');
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::REACT_TO_RELATION)]
    #[Mutation(name: 'commentOnRelation')]
    public function commentOnRelation(string $message, int $relationConnectionId): Comment {
        if (($connection = $this->em->find(
            BookRelationConnection::class,
            $relationConnectionId,
        )) instanceof BookRelationConnection) {
            $logged = $this->security->getLogged();
            assert($logged instanceof User);
            $comment = new Comment($logged, $connection, $message);
            $this->em->persist($comment);
            $this->em->flush();

            return $comment;
        }

        throw new GraphQLException(
            "BookRelationConnection with the provided id '$relationConnectionId' was not found!",
            404,
            category: 'Not Found',
        );
    }

    /**
     * @throws GraphQLException
     */
    #[Logged]
    #[Right(Rights::REACT_TO_RELATION)]
    #[Mutation(name: 'voteOnRelation')]
    public function voteOnRelation(int $value, int $relationConnectionId): Vote {
        $logged = $this->security->getLogged();
        assert($logged instanceof User);
        if (($connection = $this->em->find(
            BookRelationConnection::class,
            $relationConnectionId,
        )) instanceof BookRelationConnection) {
            if (!(($vote = $this->em->getRepository(Vote::class)->findOneBy(
                [
                    'user' => $logged,
                    'relation' => $connection,
                ],
            )) instanceof Vote)) {
                $vote = new Vote($logged, $connection, $value);
                $this->em->persist($vote);
            }

            $vote->value = $value;

            $this->em->flush();

            return $vote;
        }

        throw new GraphQLException(
            "BookRelationConnection with the provided id '$relationConnectionId' was not found!",
            404,
            category: 'Not Found',
        );
    }

    #[Logged]
    #[Right(Rights::LIST_RELATIONS)]
    #[Mutation(name: 'search')]
    public function search(string $query): Search {
        $search = new Search();
        $bookQ = $this->em->getRepository(Book::class)->createQueryBuilder('b');
        $authorQ = $this->em->getRepository(Author::class)->createQueryBuilder('a');
        $relationQ = $this->em->getRepository(BookRelation::class)->createQueryBuilder('r');
        $search->books = $bookQ->where($bookQ->expr()->like('b.title', ':query'))
            ->setParameter('query', "%$query%")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
        $search->authors = $authorQ->where($bookQ->expr()->like('a.name', ':query'))
            ->setParameter('query', "%$query%")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
        $search->relations = $relationQ->where($bookQ->expr()->like('r.name', ':query'))
            ->setParameter('query', "%$query%")
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();

        return $search;
    }

}
