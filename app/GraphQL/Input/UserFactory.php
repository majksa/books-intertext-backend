<?php declare(strict_types = 1);

namespace App\GraphQL\Input;

use App\Model\Entity\Security\User;
use JetBrains\PhpStorm\Pure;
use Maxa\Ondrej\Nette\DI\Service;
use TheCodingMachine\GraphQLite\Annotations\Factory;

#[Service]
final class UserFactory {

    public const CREATE = 'CreateUserInput';

    #[Pure]
    #[Factory(name: self::CREATE, default: true)]
    public function createUser(
        string $email,
        string $username,
        string $password,
    ): User {
        return new User($email, $username, $password);
    }

}
