<?php declare(strict_types = 1);

namespace App\GraphQL;

interface Rights {

    public const CREATE_REGISTRATION = 'CREATE_REGISTRATION';

    public const CREATE_BOOK = 'CREATE_BOOK';

    public const LIST_BOOKS = 'LIST_BOOKS';

    public const CREATE_AUTHOR = 'CREATE_AUTHOR';

    public const LIST_AUTHORS = 'LIST_AUTHORS';

    public const LIST_RELATIONS = 'LIST_RELATIONS';

    public const CREATE_RELATION = 'CREATE_RELATION';

    public const REACT_TO_RELATION = 'REACT_TO_RELATION';

}
