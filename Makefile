qa:
	composer run-script cs
	composer run-script phpstan
	make coverage

fix-and-qa:
	composer run-script cbf || true
	make qa

#####################
# LOCAL DEVELOPMENT #
#####################
compose-dev:
	docker compose -f docker-compose.local.yml up -d --build --remove-orphans

compose-dev-stop:
	docker compose -f docker-compose.local.yml down

test:
	docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 composer run-script tests

coverage:
	docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 composer run-script coverage

prepare-db: await-db
	docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 bin/console orm:schema-tool:drop --force --full-database
	docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 bin/console migrations:migrate --no-interaction
	docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 bin/console doctrine:fixtures:load --no-interaction --append

await-db:
	while ! docker exec -e NETTE_DEBUG=1 $(shell basename $(CURDIR))-app-1 bin/console orm:info ; do sleep 1 ; done > /dev/null
	echo "DB is up and running!"

migrations-diff:
	docker exec $(shell basename $(CURDIR))-app-1 bin/console migrations:diff

migrations-migrate:
	docker exec $(shell basename $(CURDIR))-app-1 bin/console migrations:migrate --no-interaction
