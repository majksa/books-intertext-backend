<?php declare(strict_types = 1);

namespace Tests;

use DG\BypassFinals;
use Nette\Configurator;
use Ninjify\Nunjuck\Environment;

if (@!include __DIR__ . '/../vendor/autoload.php') {
    echo 'Install Nette Tester using `composer update --dev`';
    exit(1);
}

Environment::setup(__DIR__);
BypassFinals::enable();

class Bootstrap {

    const ROOT = __DIR__ . '/..';

    public static function boot(): Configurator {
        $configurator = new Configurator();
        $configurator->enableTracy(self::mkdir('log'));

        return $configurator
            ->setTempDirectory(self::mkdir('temp'))
            ->addConfig(self::ROOT . '/config/env/test.neon')
            ->setDebugMode(true)
            ->addParameters([
                'rootDir' => self::ROOT,
                'appDir' => self::ROOT . '/app',
                'wwwDir' => self::ROOT . '/www',
                'testMode' => true,
            ]);
    }

    private static function mkdir(string $path): string {
        $full = __DIR__ . '/' . $path;
        if (!file_exists($full)) {
            mkdir($full, 0777, true);
        }
        return $full;
    }

}
