<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Entity\Security;

use App\Model\Entity\Security\Permission;
use App\Model\Entity\Security\PermissionBinding;
use App\Model\Entity\Security\User;
use DateTimeImmutable;
use JetBrains\PhpStorm\Pure;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../../Bootstrap.php';

/** @testCase */
final class UserTest extends BaseTestCase {

    /**
     * @dataProvider getUser
     */
    public function testBasic(User $user): void {
        Assert::equal('https://www.gravatar.com/avatar/0c83f57c786a0b4a39efab23731c7ebc', $user->getGravatar());
        $user->updateLoggedAt();
        $now = (new DateTimeImmutable())->getTimestamp();
        Assert::equal($now, $user->lastLoggedAt->getTimestamp());
    }

    /**
     * @dataProvider getUser
     */
    public function testPermissions(User $user): void {
        $permission = new Permission('permission', 'desc');
        $permission2 = new Permission('permission2', 'desc');
        Assert::true($user->getBindings()->isEmpty());
        $user->allow($permission);
        Assert::equal($permission, $user->getBindings()->get(0)->permission);
        Assert::true($user->isAllowed($permission));
        Assert::false($user->isAllowed($permission2));
        $user->deny($permission);
        Assert::false($user->isAllowed($permission));
        $user->reset($permission);
        Assert::equal(PermissionBinding::DEFAULT, $user->getBindings()->get(0)->value);
    }

    /**
     * @return array<array<User>>
     */
    #[Pure]
    public static function getUser(): array {
        return [
            [new User('email', 'username', 'password')],
        ];
    }

}

(new UserTest())->run();
