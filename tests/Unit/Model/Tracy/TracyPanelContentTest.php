<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Tracy;

use App\Model\Tracy\TracyPanelContent;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class TracyPanelContentTest extends BaseTestCase {

    public function testHtml(): void {
        $panelContent = new TracyPanelContent('title');
        $panelContent->addHtml('stuff');
        Assert::equal(
            '<div><h1>title</h1><div class="tracy-inner"><div class="tracy-inner-container">stuff</div></div></div>',
            $panelContent->toHtml(),
        );
    }

}

(new TracyPanelContentTest())->run();
