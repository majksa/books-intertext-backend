<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Database;

use App\Model\Database\Entity;
use DateTimeImmutable;
use Mockery;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class EntityTest extends BaseTestCase {

    public function testTraits(): void {
        $entity = Mockery::mock(Entity::class)->makePartial();
        $now = (new DateTimeImmutable())->getTimestamp();
        $entity->setCreatedAt();
        $entity->setUpdatedAt();
        Assert::equal($now, $entity->createdAt->getTimestamp());
        Assert::equal($now, $entity->updatedAt->getTimestamp());
    }

}

(new EntityTest())->run();
