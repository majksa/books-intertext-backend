<?php declare(strict_types = 1);

namespace Tests\Unit\Model\Security;

use App\Model\Security\FidoRegistration;
use Tester\Assert;
use Tests\Toolkit\BaseTestCase;

require __DIR__ . '/../../../Bootstrap.php';

/** @testCase */
final class FidoRegistrationTest extends BaseTestCase {

    public function testAll(): void {
        $registration = new FidoRegistration('{"id":0}', '["signatures"]');
        Assert::equal('{"id":0}', $registration->request);
        Assert::equal('["signatures"]', $registration->signatures);
    }

}

(new FidoRegistrationTest())->run();
