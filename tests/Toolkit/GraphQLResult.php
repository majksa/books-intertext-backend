<?php declare(strict_types = 1);

namespace Tests\Toolkit;

use App\Model\Utils\Arrays;
use Nette\Utils\Json;
use Tester\Assert;
use function array_shift;
use function count;
use function is_array;

final class GraphQLResult {

    /**
     * @param array<string,mixed> $data
     * @param array<mixed> $errors
     */
    public function __construct(private array $data = [], private array $errors = [], private int $code = 200) {
    }

    /**
     * @return array<string,mixed>
     */
    public function getData(): array {
        return $this->data;
    }

    /**
     * @param array<string|int>|string|int $keys
     */
    public function getItem(array|string|int $keys): mixed {
        if (!is_array($keys)) {
            $keys = [$keys];
        }

        return $this->getValue($keys, $this->data);
    }

    /**
     * @param array<string|int> $keys
     * @param array<mixed> $data
     */
    private function getValue(array $keys, array $data): mixed {
        $key = array_shift($keys);
        $data = $data[$key];
        if (is_array($data) && count($keys) > 0) {
            return $this->getValue($keys, $data);
        }

        return $data;
    }

    /**
     * @return array<array<mixed>>
     */
    public function getErrors(): array {
        return $this->errors;
    }

    /**
     * @return array<string>
     */
    public function getMessages(): array {
        return Arrays::map($this->errors, static fn ($error) => $error['message'] ?? '');
    }

    /**
     * @param array<int|string> $keys
     * @return $this
     */
    public function equal(array|string|int $keys, mixed $expected): GraphQLResult {
        Assert::equal($expected, $this->getItem($keys));

        return $this;
    }

    /**
     * @param array<string,mixed> $expected
     * @return $this
     */
    public function dataEqual(array $expected): GraphQLResult {
        Assert::equal($expected, $this->data);

        return $this;
    }

    public function notEqual(string $key, mixed $expected): GraphQLResult {
        Assert::notEqual($expected, $this->getItem($key));

        return $this;
    }

    /**
     * @param array<string,mixed> $expected
     * @return $this
     */
    public function dataNotEqual(array $expected): GraphQLResult {
        Assert::notEqual($expected, $this->data);

        return $this;
    }

    public function successful(): GraphQLResult {
        Assert::equal(0, count($this->errors));

        return $this;
    }

    public function failed(): GraphQLResult {
        Assert::notEqual(0, count($this->errors));

        return $this;
    }

    public function throws(string $expected): GraphQLResult {
        Assert::contains($expected, $this->getMessages());

        return $this;
    }

    public function status(int $expected): GraphQLResult {
        Assert::equal($expected, $this->code);

        return $this;
    }

    public function dump(): void {
        echo Json::encode([
            'data' => $this->data,
            'errors' => $this->errors,
        ], Json::PRETTY);
    }

}
