<?php declare(strict_types = 1);

namespace Tests\Toolkit;

use JetBrains\PhpStorm\Deprecated;
use Maxa\Ondrej\Nette\GraphQL\Application\Application;
use TheCodingMachine\GraphQLite\Exceptions\GraphQLExceptionInterface;
use function http_response_code;

abstract class BaseApiTestCase extends BaseContainerTestCase {

    protected Application $application;

    protected function setUp(): void {
        parent::setUp();

        $this->application = $this->container->getByType(Application::class);
    }

    /**
     * @param array<string, string> $variables
     *
     * @throws GraphQLExceptionInterface
     */
    #[Deprecated('Does not work yet how it should!')]
    protected function graphQL(string $query, array $variables = []): GraphQLResult {
        $response = $this->application->processRequest($query, $variables, false);

        return new GraphQLResult($response['data'] ?? [], $response['errors'] ?? [], http_response_code());
    }

}
