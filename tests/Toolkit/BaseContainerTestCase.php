<?php declare(strict_types = 1);

namespace Tests\Toolkit;

use Nette\DI\Container;
use Tests\Bootstrap;
use function strpos;

abstract class BaseContainerTestCase extends BaseTestCase {

    protected Container $container;

    /**
     * @param class-string<T> $class
     * @return T
     *
     * @template T of object
     */
    protected function getService(string $class): object {
        return strpos($class, '\\') ? $this->container->getByType($class) : $this->container->getService($class);
    }

    protected function setUp(): void {
        parent::setUp();

        $this->container = Bootstrap::boot()->createContainer();
    }

}
