<?php declare(strict_types = 1);

namespace Tests\Integration;

use App\Model\Database\EntityManager;
use App\Model\Entity\Security\Permission;
use App\Model\Entity\Security\User;
use App\Model\Security\AuthenticationException;
use App\Model\Security\AuthenticationService;
use App\Model\Security\AuthorizationService;
use App\Model\Security\EmailAlreadyInUseException;
use App\Model\Security\Ownable;
use App\Model\Security\Passwords;
use App\Model\Security\Security;
use App\Model\Security\UsernameTakenException;
use JetBrains\PhpStorm\Pure;
use Mockery;
use Tester\Assert;
use Tests\Toolkit\BaseContainerTestCase;
use Tests\Toolkit\InMemoryEntityManager;

require __DIR__ . '/../Bootstrap.php';

/** @testCase */
final class SecurityTest extends BaseContainerTestCase {

    private EntityManager $em;

    private Security $security;

    protected function setUp(): void {
        parent::setUp();

        $this->security = $this->getService(Security::class);
        $this->em = $this->getService(InMemoryEntityManager::class);
    }

    /**
     * @dataProvider getUser
     */
    public function testOwnable(User $user): void {
        $ownable = Mockery::mock(Ownable::class);
        $ownable->shouldReceive('getOwner')->andReturn($user);
        Assert::equal($user, $ownable->getOwner());
    }

    /**
     * @dataProvider getUser
     */
    public function testLogin(User $user): void {
        $user->id = 1;
        $username = $user->username;
        $password = $user->password;
        $duplicate = clone $user;

        Assert::null($this->security->getLogged());
        $this->security->register($user);
        $duplicate->username = 'user';
        Assert::throws(fn () => $this->security->register($duplicate), EmailAlreadyInUseException::class);
        $duplicate->username = $username;

        $duplicate->email = 'email@email.cz';
        Assert::throws(fn () => $this->security->register($duplicate), UsernameTakenException::class);

        Assert::count(1, $this->em->getUserRepository()->findAll());
        Assert::count(1, $this->security->getUsers());
        Assert::true((new Passwords())->verify($password, $this->em->getUserRepository()->find(1)->password));

        Assert::throws(
            fn () => $this->security->authenticate('test', $password),
            AuthenticationException::class,
            'User with the specified username does not exist.',
        );
        Assert::throws(
            fn () => $this->security->authenticate($username, 'test'),
            AuthenticationException::class,
            'Invalid password.',
        );

        $pass = $user->password;
        $user->password = null;
        Assert::throws(
            fn () => $this->security->authenticate($username, ''),
            AuthenticationException::class,
            'Password is not set. Please login using FIDO.',
        );
        $user->password = $pass;

        $this->security->login($user);
        Assert::equal($user, $this->security->getLogged());

        $this->security->logout();
        Assert::null($this->security->getLogged());
    }

    /**
     * @dataProvider getUser
     */
    public function testSecurity(User $user): void {
        $this->security->register($user);
        $permission = new Permission('permission', 'desc');
        $this->em->persist($permission);

        Assert::false($this->security->isUserAllowed($user, 'non existent'));
        Assert::false($this->security->isUserAllowed($user, 'permission'));
        $user->allow($permission);
        Assert::true($this->security->isUserAllowed($user, 'permission'));
    }

    /**
     * @dataProvider getUser
     */
    public function testAuthentication(User $user): void {
        $username = $user->username;
        $password = $user->password;
        $authentication = new AuthenticationService($this->security);
        Assert::false($authentication->isLogged());
        Assert::null($authentication->getUser());
        $this->security->register($user);
        $this->security->authenticate($username, $password);
        $this->security->login($user);
        Assert::true($authentication->isLogged());
        Assert::equal($user, $authentication->getUser());
    }

    /**
     * @dataProvider getUser
     */
    public function testAuthorization(User $user): void {
        $username = $user->username;
        $password = $user->password;
        $authorization = new AuthorizationService($this->security);
        Assert::false($authorization->isAllowed('any'));
        $this->security->register($user);
        $this->security->authenticate($username, $password);
        $this->security->login($user);

        $permission = new Permission('permission', 'desc');
        $this->em->persist($permission);

        Assert::false($authorization->isAllowed('non existent'));
        Assert::false($authorization->isAllowed('permission'));
        $user->allow($permission);
        Assert::true($authorization->isAllowed('permission'));
    }

    public function testTokenNotFound(): void {
        $this->security->cookie->value = 'epic-token';
        $this->security->cookie->save();
        Assert::null($this->security->getLogged());
    }

    /**
     * @return array<array<User>>
     */
    #[Pure]
    public static function getUser(): array {
        return [
            [new User('name', 'surname', 'email', 'phone', 'username', 'password')],
        ];
    }

}

(new SecurityTest())->run();
