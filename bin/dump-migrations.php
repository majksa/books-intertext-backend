#!/usr/bin/env php
<?php declare(strict_types = 1);

use Nette\Loaders\RobotLoader;

require __DIR__ . '/../vendor/autoload.php';

// Let bootstrap create Dependency Injection container.
$configurator = App\Bootstrap::boot();
$container = $configurator->createContainer();

$loader = new RobotLoader();
$loader->setTempDirectory($container->getParameters()['tempDir']);
$loader->addDirectory(__DIR__ . '/../app/Database/Migrations');
$loader->refresh();
$migrations = $loader->getIndexedClasses();
sort($migrations);
$sql = '';
foreach ($migrations as $migration) {
    $sql .= "\n# Migrating to " . basename($migration, '.php') . "\n";
    preg_match('/public function up\(Schema \$schema\) : void[^$]*\$[^\n]*\s*([^}]+)}/', file_get_contents($migration), $matches);
    $lines = explode("\n", $matches[1]);
    foreach ($lines as $line) {
        $line = trim($line);
        if ($line === '') continue;
        $line = substr($line, 15, -3);
        $sql .= str_replace('\\\'', '\'', $line) . ";\n";
    }
}

echo trim($sql);
